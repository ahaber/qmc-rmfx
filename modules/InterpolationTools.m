(* ::Package:: *)

BeginPackage["InterpolationTools`",
 {"DifferentialEquations`InterpolatingFunctionAnatomy`"}];

is$IF[x_]:=(Head[x]==InterpolatingFunction);

(* check parameter value is in domain *)
in$domain[ifn_?is$IF,x_?NumericQ,ifn$name_:""]:=Module[{dom},
 dom=InterpolatingFunctionDomain[ifn][[1]];
 If[x<dom[[1]],
  Print["x= ",CForm[x]," is outside domain ",dom," of InterpolatingFunction ",
   ifn$name];
  Return[False];
 ];
 If[x>dom[[2]],
  Print["x= ",CForm[x]," is outside domain ",dom," of InterpolatingFunction ",
   ifn$name];
  Return[False];
 ];
 True
];

(* Invert an interpolating function: *)
InverseInterpolatingFunction[if_?is$IF]:=Module[{xvals,yvals,order,dups,tempdup,j},
 If[ Length[InterpolatingFunctionDomain[if]]!=1,
  Print["ERROR: InverseInterpolatingFunction only works with 1D functions!"];
  Return[0];
 ];
 xvals = InterpolatingFunctionCoordinates[if][[1]];
 yvals = InterpolatingFunctionValuesOnGrid[if];
 order = InterpolatingFunctionInterpolationOrder[if][[1]];
 dups = dup$indices[yvals];
 xvals = Delete[xvals, dups];
 yvals = DeleteDuplicates[yvals];
 Interpolation[ Transpose[ {yvals,xvals} ], InterpolationOrder->order ]
];

(* Combine 2 interpolating functions to keep the maximum *)
(* assume they are monotonic? *)
MaxInterpolatingFunction[if1_?is$IF,if2_?is$IF]:=Module[{xvals,yvals,order},
 If[  Length[InterpolatingFunctionDomain[if1]]!=1
   || Length[InterpolatingFunctionDomain[if2]]!=1,
  Print["ERROR:  only works with 1D functions!"];
  Return[0];
 ];
 f1$xvals = InterpolatingFunctionCoordinates[if1][[1]];
 f1$yvals = InterpolatingFunctionValuesOnGrid[if1];
 f2$xvals = InterpolatingFunctionCoordinates[if2][[1]];
 f2$yvals = InterpolatingFunctionValuesOnGrid[if2];
 f1$xy = Transpose[{f1$xvals,f1$yvals}];
 f2$xy = Transpose[{f2$xvals,f2$yvals}];
 (* incomplete *)
 order = InterpolatingFunctionInterpolationOrder[if][[1]];
 Interpolation[ Transpose[ {yvals,xvals} ], InterpolationOrder->order ]
];

dup$indices[list_]:= Module[{dup, del$indices, ii, jj,tempdup},
 dup = GatherBy[Range@Length[list], list[[#]] &];
 dup = Select[dup, Length[#] > 1 &];
 del$indices = {};
 Do [
  tempdup = dup[[ii]];
  Do [del$indices = Append[del$indices, tempdup[[jj]]];
  , {jj, 1, Length[tempdup] - 1}];
 , {ii, 1, Length[dup]}
 ];
 del$indices
];

EndPackage[];
