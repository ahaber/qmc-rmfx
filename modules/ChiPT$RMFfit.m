(* ::Package:: *)

(* ::Input::Initialization:: *)
BeginPackage["ChiPT$RMFfit`"];
Needs["RMFsolverMIN`"];
Needs["Constants`"];Needs["TOV`"];
(*public functions*)
{slope$sym,RMF$ebind$PNM$fit,RMF$ebind$SYM$fit,analyze$RMF,analyze$fit,attach$crust$tab,attach$crust,offset$pressure,EPNMfunc,PPNMfunc,polyspace,M$r$curve,find$mmax,analyze$RMF2}
(*create RMF parameter table as used by RMFsolve.m from output of MultiNonlinearModelFit*)
create$paraRMF[fit_]:=Block[{gs,gw,gr,b,c,b1,zeta,fitpara,para},
fitpara=fit["BestFitParameters"];Clear[gs,gw,gr,b,c,b1,zeta];
para={{939,939},{491.5,782.5,763},{-.5,.5},{gs,gs},{gw,gw},{gr,gr},{b,c,939},{0},{0},{b1,0,0,0,0,0,0,0,0},0.16*MeV$fm^3,-1}/.fitpara;
Return[para]
]
Begin["`Private`"]
(*module to compute slopre of symmetry energy from tables {nb[MeV^4],Energy of SNM in MeV^4} and {nb[MeV^4],Energy of PNM in MeV^4}*)
slope$sym[esymtab_,epnmtab_,nsat_]:=Module[{esymfun,epnmfun,nb,symen,slope,pl},
esymfun[nb_]=Interpolation[esymtab][nb];
epnmfun[nb_]=Interpolation[epnmtab][nb];
symen[nb_]:=esymfun[nb]-epnmfun[nb];
slope=3*nsat*D[symen[x],x]/.x->nsat;
Return[slope]
]
(*function for MultiNonlinearModelFit*)

RMF$ebind$PNM$fit[nb_?NumericQ,gs_?NumericQ,gw_?NumericQ,gr_?NumericQ,b_?NumericQ,c_?NumericQ,zeta_?NumericQ,b1_?NumericQ]:=(RMF$ebind$PNM$fit[nb,gs,gw,gr,b,c,zeta,b1]=Module[{para},
para={{939,939},{491.5,782.5,763},{-.5,.5},{gs,gs},{gw,gw},{gr,gr},{b,c,939},{zeta},{0},{b1,0,0,0,0,0,0,0,0},0.16*MeV$fm^3,-1};
Return[RMFbindingPNM[nb,0,para,30,20,-8,990,False]]
]);
RMF$ebind$SYM$fit[nb_?NumericQ,gs_?NumericQ,gw_?NumericQ,gr_?NumericQ,b_?NumericQ,c_?NumericQ,zeta_?NumericQ,b1_?NumericQ]:=(RMF$ebind$SYM$fit[nb,gs,gw,gr,b,c,zeta,b1]=Module[{para},
para={{939,939},{491.5,782.5,763},{-.5,.5},{gs,gs},{gw,gw},{gr,gr},{b,c,939},{zeta},{0},{b1,0,0,0,0,0,0,0,0},0.16*MeV$fm^3,-1};
Return[Re[RMFbindingSYM[nb,0,para,30,20,-8,990,False]]]
])

(*analyze any RMF of the form suitable as input for RMFsolve*)
analyze$RMF[para_]:=Module[{nbrestabPNM,nbrestabSNM,fitpara,presstabSNM,presstabPNM,EbindtabPNM,EbindtabSNM,i,pl1,pl2,nucdens,nbrestabSNMn0,enertab,incomp$sat,nbsat,x,u,Esat,esym,slopesym,esymfun},

nbrestabPNM=RMFsolve$nb$PNM[0.5*NuclearDensity$nucleons$MeV3,2*NuclearDensity$nucleons$MeV3,30,0,para,30,20,-3,990,False];
nbrestabSNM=RMFsolve$nb$SYM[0.5*NuclearDensity$nucleons$MeV3,2*NuclearDensity$nucleons$MeV3,30,0,para,30,20,990,False];
EbindtabPNM=Table[{nbrestabPNM[[i,1]],binding$energy$RMF[nbrestabPNM[[i,2]]]},{i,1,Length[nbrestabPNM]}];
EbindtabSNM=Table[{nbrestabSNM[[i,1]],binding$energy$RMF[nbrestabSNM[[i,2]]]},{i,1,Length[nbrestabSNM]}];
nbsat=u/.FindRoot[(D[Interpolation[EbindtabSNM][x],x]/.x->u)==0,{u,0.98*NuclearDensity$nucleons$MeV3}];
Esat=Interpolation[EbindtabSNM][nbsat];
esymfun[x_]=Interpolation[EbindtabPNM][x]-Interpolation[EbindtabSNM][x];
nbrestabSNMn0=RMFsolve$nb$SYM[0.999*nbsat,1.001*nbsat,2,0,para,30,20,990,False];
enertab=Table[{nbrestabSNMn0[[i,1]],edens$RMF[nbrestabSNMn0[[i,2]]]},{i,1,Length[nbrestabSNMn0]}];
incomp$sat=9*nbsat*(Chop[enertab[[3,2]]-2enertab[[2,2]]+enertab[[1,2]]])/(Chop[enertab[[1,1]]-enertab[[2,1]]])^2;
slopesym=3*nbsat*D[esymfun[x],x]/.x->nbsat;
Return[{nbsat/MeV$fm^3,Esat,incomp$sat,esymfun[nbsat],slopesym}]
];
(*analyze fit as provided by MultiNonlinearModelFit *)
analyze$fit[fit_]:=Module[{nbrestabPNM,nbrestabSNM,fitpara,para,presstabSNM,presstabPNM,EbindtabPNM,EbindtabSNM,i,pl1,pl2,nucdens,nbrestabSNMn0,enertab,incomp$sat,nbsat,x,u,Esat,esym},
fitpara=fit["BestFitParameters"];Clear[gs,gw,gr,b,c,b1,zeta];
para={{939,939},{491.5,782.5,763},{-.5,.5},{gs,gs},{gw,gw},{gr,gr},{b,c,939},{0},{0},{b1,0,0,0,0,0,0,0,0},0.16*MeV$fm^3,-1}/.fitpara;
nucdens=para[[11]];
nbrestabPNM=Chop[RMFsolve$nb$PNM[0.5*NuclearDensity$nucleons$MeV3,2*NuclearDensity$nucleons$MeV3,30,0,para,30,20,-3,990,False]];
nbrestabSNM=Chop[RMFsolve$nb$SYM[0.5*NuclearDensity$nucleons$MeV3,2*NuclearDensity$nucleons$MeV3,30,0,para,30,20,990,False]];
EbindtabPNM=Chop[Table[{nbrestabPNM[[i,1]]/NuclearDensity$nucleons$MeV3,binding$energy$RMF[nbrestabPNM[[i,2]]]},{i,1,Length[nbrestabPNM]}]];
EbindtabSNM=Chop[Table[{nbrestabSNM[[i,1]]/NuclearDensity$nucleons$MeV3,binding$energy$RMF[nbrestabSNM[[i,2]]]},{i,1,Length[nbrestabSNM]}]];

nbrestabSNMn0=Chop[RMFsolve$nb$SYM[0.999*nucdens,1.001*nucdens,2,0,para,30,20,990,False]];
enertab=Table[{nbrestabSNMn0[[i,1]],edens$RMF[nbrestabSNMn0[[i,2]]]},{i,1,Length[nbrestabSNMn0]}];
incomp$sat=9*nucdens*(Chop[enertab[[3,2]]-2enertab[[2,2]]+enertab[[1,2]]])/(Chop[enertab[[1,1]]-enertab[[2,1]]])^2;
nbsat=u/.FindRoot[(D[Interpolation[EbindtabSNM][x],x]/.x->u)==0,{u,0.9}];
Esat=Interpolation[EbindtabSNM][nbsat];
esym=Interpolation[EbindtabPNM][nbsat]-Interpolation[EbindtabSNM][nbsat];
Return[{nbsat,Esat,incomp$sat,esym,slope$sym[EbindtabSNM,EbindtabPNM,nbsat]}]
];
(*input: EOS table for RMF, for crust, nb(mu) table of RMF (can be rewritten s.t. this table is included in eos$RMF), offset pressure+bag constant)*)
offset$pressure[eos$RMF_,eos$crust_,nb$of$muB$RMF_,Poff_]:=Module[{k,mu$of$P$RMF,mu$of$P$RMFfun,pcross,p,nbcr,mu$of$P$RMFoff,mu$of$P$crfun,nb$of$mu$crfun},
mu$of$P$RMF=eos$RMF[[All,{1,3}]];
mu$of$P$RMFoff={#1+Poff,#2}&@@@mu$of$P$RMF;
mu$of$P$RMFfun=Interpolation[mu$of$P$RMFoff];
mu$of$P$crfun=Interpolation[eos$crust[[All,{2,3}]]];
nb$of$mu$crfun=Interpolation[eos$crust[[All,{3,1}]]];
pcross=p/.FindRoot[{mu$of$P$crfun[p]-mu$of$P$RMFfun[p]==0},{p,100000,1,10000000}];
nbcr=nb$of$mu$crfun[mu$of$P$RMFfun[pcross]]/MeV$fm^3/0.16;
Return[{pcross,mu$of$P$crfun[pcross],mu$of$P$RMFfun[pcross],nbcr}]
];

(*module that combines crust+core at a given transitoin crust baryon denisity (first order phase transition) where mu_B and P are smoothly connected at the phase transitoin and n_B is a monotonic function of mu_B*)
attach$crust[nbtrans_,eos$RMF_,eos$crust_,nb$of$muB$RMF_]:=Module[{Poff,po,epsofP$RMF,eps$of$P,p,epsofP$RMF$fun,pcross,mu,nb$of$mu,e$of$pcrfun},
po=Poff/.FindRoot[offset$pressure[eos$RMF,eos$crust,nb$of$muB$RMF,Poff][[4]]==nbtrans,{Poff,-0.5*10^6},Evaluated->False][[1]];
epsofP$RMF={#1+po,#2+po}&@@@eos$RMF[[All,{1,2}]];
epsofP$RMF$fun=Interpolation[epsofP$RMF,InterpolationOrder->1];
pcross=offset$pressure[eos$RMF,eos$crust,nb$of$muB$RMF,po];
e$of$pcrfun[p_]=Interpolation[eos$crust[[All,{2,5}]],InterpolationOrder->1][p];
eps$of$P[p_]=If[p<=pcross[[1]],e$of$pcrfun[p],epsofP$RMF$fun[p]];
Return[{eps$of$P,epsofP$RMF[[-1,1]],pcross,po}]; 
];(*output is interpolating function, range of validity i.e. maxiumum pressure, transition pressure in MeV^4 and bag constant in MeV^4*)

(*some module as above, but output is a table *)
attach$crust$tab[nbtrans_,eos$RMF_,eos$crust_,nb$of$muB$RMF_]:=Module[{Poff,po,epsofP$RMF,eps$of$P,p,epsofP$RMF$fun,pcross,mu,nb$of$mu,e$of$pcrfun,epsofP$cr,l1,l2,eps$of$P$joined},
po=Poff/.FindRoot[offset$pressure[eos$RMF,eos$crust,nb$of$muB$RMF,Poff][[4]]==nbtrans,{Poff,-0.5*10^6},Evaluated->False][[1]];
epsofP$RMF={#1+po,#2+po}&@@@eos$RMF[[All,{1,2}]];
epsofP$cr=eos$crust[[All,{2,5}]];
pcross=offset$pressure[eos$RMF,eos$crust,nb$of$muB$RMF,po];
l1=Select[epsofP$cr,#[[1]]<pcross[[1]]&];
l2=Select[epsofP$RMF,#[[1]]>pcross[[1]]&];
eps$of$P$joined=Join[l1,l2];

Return[{eps$of$P$joined,epsofP$RMF[[-1,1]],pcross,po}];];
(*a b alpha beta parametrization to pick curves in chiPT band and generate binding energy + pressure from it*)
n0=0.16;
EPNMfunc[n_,aval_,alphaval_,bval_,betaval_]:=aval*(n/n0)^alphaval+bval*(n/n0)^betaval;
PPNMfunc[n_,aval_,alphaval_,bval_,betaval_]:=aval*alphaval*n*(n/n0)^alphaval+bval*betaval*n*(n/n0)^betaval;

(*function that finds maximum of an M-R curve, as well as R(1.4Msun), R(Mmax) and checks whether curve has reached a maximum (topped=1) or not (topped=0*)
(*input Table[{R[km],M/Msun}]*)

find$mmax[mrtab_]:=Module[{Rmmax,R,x,rad,mmax,R14,topped,mrfun,mrtab2,rmtab},
mrtab2=Select[mrtab,#[[2]]>=1.7&];
rmtab=mrtab[[All,{2,1}]];
mrfun[x_]=Interpolation[mrtab2,InterpolationOrder->3][x];
Rmmax=x/.FindRoot[(D[mrfun[R],R]/.R->x)==0,{x,11}];
mmax=mrfun[Rmmax];
R14=Interpolation[rmtab][1.4];
If[Max[mrtab[[All,2]]]==mrtab[[-1,2]],topped=0,topped=1];
Return[{mmax,Rmmax,R14,topped}];
];
(*these modules help computing the M-R curves and rely on the TOV package by Mark Alford*)
polyspace[n_,increments_,start_,end_]:=Module[{a},(a=Range[0,increments];
(a/increments*((end-start)+1)^(1/n))^(n)-1+start)];
(*cutoff P should be lowest pressure entry for best results*)
(*ptab=table of central pressures,E$ofP = eos function*)

M$r$curve[ptab_,E$of$P_,cutoffP_]:=Module[{tov,mrtab,i},
mrtab={};
Do[(
tov=create$TOV[ptab[[i]],E$of$P];
solve$TOV[tov,{Pfrac->10^(-8),Psurface->cutoffP}];
mrtab=Append[mrtab,{ptab[[i]],radius$km[tov],mass$solar[tov]}];),{i,1,Length[ptab]}];
Return[mrtab]
];


End[];
EndPackage[];
